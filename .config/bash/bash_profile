# ~/.bash_profile
# vim: ft=bash

export BASHDOTDIR="${HOME}/.config/bash"

_use_bashrc_on_login() {
	# Esto hace que para shells logins también se ejecute el .bashrc si
	# existe y no solo para shells no-logins interactivas o remotas
	[[ -r "$BASHDOTDIR/bashrc" ]] && . "$BASHDOTDIR/bashrc"
}

# Directorios XDG:
source "${BASHDOTDIR}/variables/xdg.sh"

# Historial
#export HISTSIZE=0                           # No historial
HISTCONTROL=erasedups:ignoreboth             # En caso uses historial, ignora duplicados y comandos que empiezan con espacio
HISTFILE="${XDG_DATA_HOME}/bash/history"     # En caso uses historial, ponlo en esa ruta
HISTIGNORE="&:[ ]*:exit:e:clear:c:ls:ll:la:history" \
HISTTIMEFORMAT='%m/%d %T '

# Extra variables for my personal use like $SCRIPTS
source "${BASHDOTDIR}/variables/extra.sh"

# Locale
source "${BASHDOTDIR}/variables/locale.sh"

# Aplicaciones por omisión
source "${BASHDOTDIR}/variables/default_apps.sh"

# XDG paths for shells' rcs
source "${BASHDOTDIR}/variables/xdgshells.sh"

# Principales variables de entorno
source "${BASHDOTDIR}/variables/configs.sh"

# Variables para Xorg y Unix en general
source "${BASHDOTDIR}/variables/unix.sh"

# Variables para método de entrada: fcitx5
source "${BASHDOTDIR}/variables/im.sh"

# Colores
source "${BASHDOTDIR}/variables/colors.sh"
source "${BASHDOTDIR}/variables/lscolors.sh"

# Configuración fzf
source "${BASHDOTDIR}/variables/fzf.sh"

# Envolvedores de programas
source "${BASHDOTDIR}/variables/wrappers.sh"

# Variables de entorno para lenguajes de programación:
source "${BASHDOTDIR}/variables/proglangs.sh"

# Directorios no-XDG:
source "${BASHDOTDIR}/variables/xdgfix.sh"

# En /etc/bash.bashrc está lo siguiente:
#BASHDOTDIR=${XDG_CONFIG_HOME:-$HOME/.config}/bash
#if [[ $(shopt login_shell) = *on* ]]; then
#	[ -f "${BASHDOTDIR}/bash_profile" ] && . "${BASHDOTDIR}/bash_profile"
#else
#	[ -f "${BASHDOTDIR}/bashrc" ] && . "${BASHDOTDIR}/bashrc"
#fi
