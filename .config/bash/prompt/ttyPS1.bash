bash_prompt_command() {
	local \
		pwdmaxlen="${1:-25}" \
		trunc_symbol="${2:-"…"}" \
		home_symbol="${3:-""}" \
		dir=${PWD##*/}
	local -i \
		dirlen="${#dir}" \
		pwdoffset

	if (( pwdmaxlen < dirlen )); then
		dir="${dir::3}${trunc_symbol}"
		pwmaxlen="$((dirlen + ${#dir}))"
	fi

	NEW_PWD="${PWD/#"$HOME"/"$home_symbol"}" \
	NEW_PWD="${NEW_PWD%/*}/${dir}" \
	pwdoffset=$(( ${#NEW_PWD} - pwdmaxlen ))

	if (( pwdoffset > 0 )); then
		NEW_PWD="${NEW_PWD:$pwdoffset:$pwdmaxlen}" \
		NEW_PWD="${trunc_symbol}/${NEW_PWD#*/}"
	fi
}

bash_prompt() {
	local PROMPT_PWD='${NEW_PWD}'
	# En caso esté en un pts, pon los triángulos
	if [[ "$(tty)" == '/dev/pts/'* ]]; then
		PS1="\[\033]0;\u:${PROMPT_PWD}\007\]\[\033[0;1;37;48;2;39;0;138m\] \u \[\033[0;41;38;2;39;0;138m\]\[\033[0;97;1;41m\] ${PROMPT_PWD} \[\033[0;31;49m\]\[\033[1;38;5;47m\] "
	else
		PS1="\[\033]0;\u:${PROMPT_PWD}\007\]\[\033[0;1;37;48;2;39;0;138m\] \u \[\033[0;97;1;41m\] ${PROMPT_PWD} \[\033[0m\] \[\033[1;38;5;47m\]"
	fi
}

PROMPT_COMMAND='bash_prompt_command 25 ".." "~"'
bash_prompt
unset -f bash_prompt
