#    ██                        ██
#   ░██                       ░██
#   ░██       ██████    ██████░██      ██████  █████
#   ░██████  ░░░░░░██  ██░░░░ ░██████ ░░██░░█ ██░░░██
#   ░██░░░██  ███████ ░░█████ ░██░░░██ ░██ ░ ░██  ░░
# ██░██  ░██ ██░░░░██  ░░░░░██░██  ░██ ░██   ░██   ██
#░██░██████ ░░████████ ██████ ░██  ░██░███   ░░█████
#░░ ░░░░░    ░░░░░░░░ ░░░░░░  ░░   ░░ ░░░     ░░░░░

BASH_LOADABLES_PATH="/usr/local/lib/bash:/usr/lib/bash:/opt/local/lib/bash:/usr/pkg/lib/bash:/opt/pkg/lib/bash:."

# Si no corre interactivamente, no hagas nada,
# Si corre interactivamente, usa ble.sh:
if [[ $- == *i* ]]; then
	source /usr/share/blesh/ble.sh --noattach
fi

# Preconfiguración principal: {{{

export BASHDOTDIR="${XDG_CONFIG_HOME}/bash"      # Directorio de los ficheros perfil de bash
export BASHRC="${BASHDOTDIR}/bashrc"             # Lugar del bashrc

# Evitar y eliminar duplicados en PATH:
source "${BASHDOTDIR}/functions/path_append"
source "${BASHDOTDIR}/functions/path_prepend"

# Historial
#HISTSIZE=0                               # No historial
HISTCONTROL=erasedups:ignoreboth          # En caso uses historial, ignora duplicados y comandos que empiezan con espacio
HISTFILE="${XDG_DATA_HOME}/bash/history"  # En caso uses historial, ponerlo en esa ruta
HISTIGNORE="&:[ ]*:exit:e:clear:c:ls:ll:la:history" \
HISTTIMEFORMAT='%m/%d %T ' \
TIMEFORMAT=$'\e[;1;31m{
  \e[34m"real"\e[31m: \e[3;32m"%6lR"\e[;1;31m,
  \e[34m"user"\e[31m: \e[3;32m"%6lU"\e[;1;31m,
  \e[34m"sys"\e[31m : \e[3;32m"%6lS"
\e[;1;31m}\e[m'

SCRIPTS="$HOME/.local/bin/guiones"

SHELL_CURRENT="$(readlink "/proc/$$/exe")"   # Shell actual (si es POSIX compatible)
TERMINAL_CURRENT="$("${SCRIPTS}/terminal")"  # Terminal actual

#Columnas para 'ps', o sea correr 'ps -eo $pscols'
#Columnas para 'findmnt' o sea correr 'findmnt -o $fscols'
pscols="pid,user,stat,pcpu,pmem,comm,cmd" \
fscols="SOURCE,TARGET,FSTYPE,SIZE,USED,USE%" \
datef='+%d/%m/%Y %H:%M:%S'

if [[ -f "$BASHDOTDIR/.tmp.sh" ]]; then
	source "$BASHDOTDIR/.tmp.sh"
fi

if [[ -f "$BASHDOTDIR/variables/.private.sh" ]]; then
	source "$BASHDOTDIR/variables/.private.sh"
fi

path_append "${XDG_DATA_HOME%/*}/bin"      # Ejecutables locales al usuario (~/.local/bin)
path_append "${SCRIPTS}"                   # Mis scripts que quiero que estén en path (CUIDADO, el PATH se está agregando antes, puede ser inseguro en un servidor)

if [ "$ONTTY" ]; then                                # Si estás en un tty (establecido desde /etc/profile):
	source "$BASHDOTDIR/prompt/ttyPS1.bash"   # Acá está mi indicador de Bash que corre en ttys
else
	#builtin eval "$(starship init bash)"            # Indicador starship
	source "$BASHDOTDIR/prompt/prompt_logo_minimal.sh"
fi

#set -o vi                      # Si quieres modo vi de Bash
set -o emacs                    # Si quieres modo emacs de Bash (el modo por omisión)

# Lenguaje
#export LC_MESSAGES=C          # Puedes poner solo esta, y si no funciona ↓. "C" para ver en inglés
export LANG=es_PY.UTF-8        # O estas dos, que cambian todo el lenguaje de tu equipo
#export LANGUAGE=es_PY:en
#}}}

# Aliases y variables generales: {{{

# Para edición de ficheros comunes

shopt -s autocd   # Para auto-cd: cambiar de directorio sin tener que usar cd
shopt -s globstar # Glob '**' empareja recursivamente debajo
shopt -s cdspell  # cd a directorio aun si deletraste mal 1 letra
shopt -s cdable_vars  # cd a nombres de variables, sin tener que poner '$'

# Aliases

source "$BASHDOTDIR/aliases/colorizer.bash"
source "$BASHDOTDIR/aliases/main.sh"
source "$BASHDOTDIR/aliases/main.bash"
source "$BASHDOTDIR/aliases/rc.sh"
source "$BASHDOTDIR/aliases/nonposix.sh"
source "$BASHDOTDIR/aliases/xdgfix.sh"

#}}}

# Funciones interactivas: {{{

## Plugins: {{{

# Para z.lua
builtin eval -- "$(luajit "${HOME}/.local/bin/z.lua" --init bash enhanced once)"
source "$BASHDOTDIR/plugins/zlua.bash"

# Para g.sh
#source "$BASHDOTDIR/plugins/gsh.sh"
source /usr/share/g/g_source.sh

# Para broot
builtin eval -- "$(broot --print-shell-function bash)"

# Para lf + cd
source "$BASHDOTDIR/functions/lfcd"

## }}}

source "$BASHDOTDIR/functions/fhis"    # Función para ver historial con fzf
source "$BASHDOTDIR/functions/fcd"     # Función para hacer cd con fzf
source "$BASHDOTDIR/functions/fcdd"    # fcd, pero con cdd
source "$BASHDOTDIR/functions/cdd"     # Función que es solo pushd sin stdout
source "$BASHDOTDIR/functions/cdf"     # cdd con fzf, sobre directorios en dirs
source "$BASHDOTDIR/functions/cls"     # cd y ls a la vez
source "$BASHDOTDIR/functions/cll"     # cd y ll a la vez
source "$BASHDOTDIR/functions/cla"     # cd y la a la vez
source "$BASHDOTDIR/functions/rms.bash"
source "$BASHDOTDIR/functions/mkcd"
source "$BASHDOTDIR/functions/print.sh"
source "$BASHDOTDIR/functions/printz.bash"
source "$BASHDOTDIR/functions/pprint.bash"
source "$BASHDOTDIR/functions/up"      # Subir directorios # veces
source "$BASHDOTDIR/functions/unbind"  # Desdefinir variable solo lectura
source "$BASHDOTDIR/functions/evalhd.bash"
source "$BASHDOTDIR/functions/echohd.bash"  #Imprimir heredoc, sin cat
source "$BASHDOTDIR/functions/funcbat" # Ver funciones bash con bat
source "$BASHDOTDIR/functions/funced"  # Editar funciones
source "$BASHDOTDIR/functions/arr"     # Crear arreglos POSIX
source "$BASHDOTDIR/functions/varcheck"
source "$BASHDOTDIR/functions/assign"

#}}}

# Accesos rápidos con Readline y Ble: {{{
#bind "set completion-ignore-case on"
#bind "set show-all-if-ambiguous on"
#bind "set completion-map-case on"
source "$BASHDOTDIR/binds/ble-binds.bash"
source "$BASHDOTDIR/binds/main.bash"

source "$BASHDOTDIR/functions/run_help.bash"
source "$BASHDOTDIR/functions/run_chtsh.bash"
source "$BASHDOTDIR/functions/run_editor.bash"
source "$BASHDOTDIR/functions/run_copy.bash"
source "$BASHDOTDIR/functions/run_paste.bash"
source "$BASHDOTDIR/functions/run_rename.bash"
#run_cd() { fcd "${READLINE_LINE}"; }
#run_print() { xclip -o; }
#read_readline(){ REPLY=$($READLINE_LINE);}
#write_readline(){ READLINE_LINE="$REPLY";}
run_terminal() { terminal-samewd -t;}  # New terminal in same directory
# Esto genera globbings y emparejamientos insensibles a mayúsculas (pero ble.sh ya tiene una opción que reemplazaría eso)
#shopt -s nocasematch
#shopt -s nocaseglob

#}}}

[[ ${BLE_VERSION-} ]] && ble-attach
