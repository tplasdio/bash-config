# Con bind de ble, el PWD de la barra de vim-airline sí se actualiza, pero no puedo usar "$READLINE_LINE"
ble-bind -c 'M-,' 'br'
ble-bind -c 'M-;' 'fcd'
ble-bind -c 'M-.' 'lfcd'
ble-bind -m 'vi_nmap' -c 'M-.' 'lfcd'
ble-bind -m 'vi_nmap' -c 'M-,' 'br'
ble-bind -m 'vi_nmap' -c 'M-;' 'fcd'
ble-bind -m 'vi_nmap' -c 'M-E' 'ffd -fpe +m --header "Ficheros debajo"'
ble-bind -c 'M-f' "$_GS_CMD"' -I'
ble-bind -c 'M-z' 'zh'
ble-bind -m 'vi_nmap' -c 'M-f'  "$_GS_CMD"' -I'
ble-bind -m 'vi_nmap' -c 'M-z' 'zh'
ble-bind -c 'M-h' 'run_help'
ble-bind -c 'M-H' 'fhis'

ble-bind -c 'M-L' 'cd ..'
ble-bind -m 'vi_nmap' -c 'M-L' 'cd ..'
ble-bind -c 'M-{' 'ls -Alh'
ble-bind -m 'vi_nmap' -c 'M-{' 'ls -Alh'
