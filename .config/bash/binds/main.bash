# Readline binds
declare -A acc_rapidos=(
	['h']="run_help"
	['g']="run_chtsh"
	['s']="scripts"
	['e']="run_editor -0"
	['E']='ffd -fpe +m --header "Ficheros debajo"'
	['p']="run_copy"
	['P']="run_paste"
	['n']="run_terminal"
	['r']="rgfzf -dp"
	['R']="rgfzf -dp --rg=rga --depth 1"
	['b']="run_rename"
	['B']="run_rename --deep" )
	#['z']="run_cd"
	#['o']="run_print"

# Alt+tecla para correr la función
for _ in "${!acc_rapidos[@]}"; do
	bind -m vi-insert -x "\"\033${_}\": ${acc_rapidos["${_}"]}"
	bind -m emacs	  -x "\"\033${_}\": ${acc_rapidos["${_}"]}"
done
