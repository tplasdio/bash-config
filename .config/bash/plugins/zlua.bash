function _zi {
	#https://github.com/skywind3000/z.lua/wiki/Effective-with-fzf#_zl_fzf_flag
	# z interactivo personalizado
	local dir="$(z -l "$@" \
		| \fzf --height=$((LINES-1)) --nth 2.. --tac +s -e --header "Directorios frecientes" --header-first \
		--preview "lsd --color always --icon always --tree --depth 3 --group-dirs first {+2} 2>/dev/null")"
	if [ "$dir" ]; then
		dir="${dir#* }" dir="${dir##+([[:space:]])}"  # Second field
		cd -- "$dir"
	else
		return 1
	fi
}

export \
_ZL_DATA="${XDG_DATA_HOME}/z/.zlua" \
_ZL_NO_PROMPT_COMMAND=1 \
_ZL_MAXAGE="10000"

alias \
zh='_zi -t .' \
zp='z -p' \
# con push

alias zhp='z -I -t -p .'  # con push  # Bug: no funciona con mi _zi personalizado, sí con z -I
