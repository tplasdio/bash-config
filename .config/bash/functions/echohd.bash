# faster than /bin/cat, slower than a builtin cat
# This is significantly slower on bash <= 2.01.1 (1997)
# and very significantly faster on bash >= 5.2 (2022)
echohd() {
	builtin printf -- "%s" "$(</dev/stdin)"
}
