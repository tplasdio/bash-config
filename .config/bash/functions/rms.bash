# rm functions

# rm wrapper to hopefully prevent "rm ~" or "rm /"
rm(){
	case "$2" in
	(~|/|"$HOME") return 2;;
	(.)
		case "$(realpath . || readlink -f .)" in
		(~|/|"$HOME") return 2;;
		esac;;
	(*) /bin/rm "$@"
	esac
}

# Safely remove file/directory named '~'
rm~(){ /bin/rm -r -- ./'~';}

# Shred instead of rm (WARNING: by default truncates symlinks that resolve to regular files)
rmm(){ shred -u "$@" &}
