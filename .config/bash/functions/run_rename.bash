run_rename(){
	#set -o extglob
	case "$1" in
	(-d|--deep)  find . ! -path . -printf '%P\0' | \xargs -0 vimv ;;
	(*) vimv ?(.)[!.]* ;;
	esac
}
