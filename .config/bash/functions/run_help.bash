# Open a manual, --help, info, or help of a command
# With no arguments, open 'fman' manual selector
run_help() {
	if [ ! "${READLINE_LINE-a}" ]; then
		hash fman >/dev/null 2>&1 && fman -m \
			|| return $?
	else
		# Bugs: When suspending manual with Ctrl-z, the command line is nevertheless executed with --help
		man "${READLINE_LINE}" \
			|| ${READLINE_LINE} --help \
			|| {
				local info=$(info -w "${READLINE_LINE}")
				[ "${info}" ] \
					&& "${EDITOR}" "${info}" \
					|| help "${READLINE_LINE}" 2>/dev/null
			}
	fi
}
