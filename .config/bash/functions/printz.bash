# Print stdin or string arguments as next command line
# Similar to print -z from zsh in bash+ble.sh
printz(){
	if [[ $# -eq 0 || "$1" = - ]]; then
		ble append-line "$(</dev/stdin)"
	else
		ble append-line "$*"
	fi
}
