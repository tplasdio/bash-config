# Show a cheatsheet of a command
run_chtsh() {
	local command
	if [ ! "${READLINE_LINE-a}" ]; then
		command=$(compgen -c | fzf) || return 1
	else
		command="${READLINE_LINE}"
	fi
	printf "\033[;1;34m========> \033[32mLocal cheatsheet \033[;1;34m<========\033[m\n"
	cheat "${command}"
	printf "\033[;1;34m<==================================>\033[m\n"
	cht.sh "${command}" 2>/dev/null \
		|| curl "cht.sh/${command}" 2>/dev/null
}
