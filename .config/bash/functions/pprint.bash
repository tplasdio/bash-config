# pretty-print array or associative array
# TODO:
# - rewrite as builtin
# - more options: yaml, xml, declare -p indented
pprint() {
	local -i json
	case "$1" in
	(-j | --json)
		json=1
		shift
	esac
	local k

	while [[ $1 ]]; do
		local -n _A="$1"
		shift

		case "${_A@a}" in
		(*a*)
			if ((json)); then
				printf "["
				printf '"%s"' "${_A[0]}"
				for k in "${_A[@]:1}"; do
					printf ',"%s"' "${k}"
				done
				echo "]"
			else
				for k in "${_A[@]}"; do
					printf "\e[1;32m[\e[m%s\e[1;32m]\n" "${k//\"/\\\"}"
				done
			fi
		;;
		(*A*)
			if ((json)); then
				echo "{"
				local -i i=0
				for k in "${!_A[@]}"; do
					if ((i)); then
						printf ',\n  "%s": "%s"' "${k//\"/\\\"}" "${_A["$k"]//\"/\\\"}"
					else
						printf '  "%s": "%s"' "${k//\"/\\\"}" "${_A["$k"]//\"/\\\"}"
					fi
					((i+=1))
				done
				printf "\n}\n"
			else
				for k in "${!_A[@]}"; do
					printf "\e[1;32m[\e[m%s\e[1;32m] [\e[m%s\e[1;32m]\e[m\n" "${k}" "${_A["$k"]}"
				done
			fi
		esac
	done
}
