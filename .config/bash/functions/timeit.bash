# Time with nanoseconds
# Use a builtin `date` command to diminish imprecision (~3ms on my system)
# of starting a file command (e.g. /bin/date)
function timeN {
	local -i t0 t1 es
	local IFS DATE td
	unset -v IFS
	: "${DATE:="date +%s%N"}"
	t0="${ $DATE; }"
	REPLY=${| "$@"; }
	es=$? t1="${ $DATE; }" \
	td=$((t1-t0)) \
	td=${ printf -- "%09d" "$td"; }
	>&2 printf -- "%s\n" "${td::-9}.${td: -9}"
	return $es
}

# Time with microseconds
# Since it uses a bash special variable, there's less imprecision
function timeM {
	local -i t0 t1 es
	local td
	t0=${EPOCHREALTIME//[,.]}
	REPLY=${| "$@"; }
	es=$? t1=${EPOCHREALTIME//[,.]} \
	td=$((t1-t0)) \
	td=${ printf -- "%06d" "$td"; }
	>&2 printf -- "%s\n" "${td::-6}.${td: -6}"
	return $es
}
