run_editor() {
	case "$1" in
	(-0)
		# Cuidado al usar nano! Se rellena poco a poco
		"${EDITOR}" 0< <( builtin eval -- " ${READLINE_LINE}" )
	;;
	(-v)
		# puedes usar mi script shvipe o vipe de moreutils
		builtin eval -- " ${READLINE_LINE} | shvipe"
	;;
	(*)
		# Solo para comandos unilínea, no usar con un programa interactivo!! (e.g. fzf)
		"${EDITOR}" <( builtin eval -- " ${READLINE_LINE}" )
	esac
}
