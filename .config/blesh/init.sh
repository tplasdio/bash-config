#    ██       ██
#   ░██      ░██
#   ░██      ░██  █████  ██████  █████
#   ░██████  ░██ ██░░░██░░██░░█ ██░░░██
#   ░██░░░██ ░██░███████ ░██ ░ ░██  ░░
# ██░██  ░██ ░██░██░░░░  ░██   ░██   ██
#░██░██████  ███░░██████░███   ░░█████
#░░ ░░░░░   ░░░  ░░░░░░ ░░░     ░░░░░
## Este es un ejemplo del fichero ~/.blerc ó ~/.config/blesh/init.sh
##
## Para poder usar ble.sh con bash debe poner las siguientes líneas
## en su ~/.bashrc:
##
## Al inicio del .bashrc coloque la ruta a ble.sh:
## [[ $- == *i* ]] && source ~/.local/share/blesh/ble.sh --noattach
##
## Al final del .bashrc coloque esto para adjuntar ble:
## [[ ${BLE_VERSION-} ]] && ble-attach
## Importar blesh me toma ~7ms mas al iniciar el shell, es poco

##-----------------------------------------------------------------------------
## Configuraciones

############################
###### Abreviaciones #######
############################

#INIAB
ble-sabbrev \
	0.='-0 | xargs -0' \
	1.='1>/dev/null' \
	1.2='3>&1 1>&2 2>&3 3>&-' \
	2.1='3>&1 1>&2 2>&3 3>&-' \
	2.='2>/dev/null' \
	3.='&>/dev/null' \
	.2='../../' \
	.3='../../../' \
	.4='../../../../' \
	b.='| rg bin/' \
	real.='realpath $(which' \
	ed.='"$EDITOR" $(which' \
	edf.='"$EDITOR" ${BASHFUNCTIONS}/' \
	edl.='"$EDITOR" ${SCRIPTSLIB}/' \
	bat.='bat $(which' \
	batf.='bat ${BASHFUNCTIONS}/' \
	fzfb.='fzf --height=$(($(tput lines)-1)) --preview "bat --color=always --line-range :100 {}"' \
	tmp.='> "${tmp=$(mktemp)}"' \
	tmps.='> "${TMPS[${#TMPS[@]}]=$(mktemp)}"' \
	chmod.='chmod u+x' \
	chmod-='chmod u-x' \
	v='"${EDITOR}"' \
	T='dem "$TERMINAL" -e' \
	V='dem "$TERMINAL" -e "${EDITOR}"' \
	ve='teditor' \
	g='git' \
	gf='git fz' \
	gs='git sss' \
	gl='git l' \
	gb='git b' \
	gz='git z' \
	ga='git add' \
	g1='git l1' \
	gc.="git commit -m '" \
	ls-tree.='ls-tree --full-tree --name-only -r HEAD' \
	lola.='log --graph --decorate --pretty=oneline --abbrev-commit --all' \
	icat='kitty +kitten icat'
	#'\alpha=α' \
	#'\beta=β' \
	#'\gamma=γ' \
	#'\delta=δ' \
	#'\Delta=Δ' \
	#'\epsilon=ε' \
	#'\zeta=ζ' \
	#'\eta=η' \
	#'\theta=θ' \
	#'\iota=ι' \
	#'\kappa=κ' \
	#'\lambda=λ' \
	#'\mu=μ' \
	#'\nu=ν' \
	#'\xi=ξ' \
	#'\omicron=ο' \
	#'\pi=π' \
	#'\rho=ρ' \
	#'\sigma=σ' \
	#'\varsigma=ς' \
	#'\Sigma=Σ' \
	#'\tau=τ' \
	#'\phi=ɸ' \
	#'\varphi=φ' \
	#'\chi=χ' \
	#'\psi=ψ' \
	#'\omega=ω' \
	#'\Omega=Ω' \
	#'\euler=ℯ' \
	#'\sqrt=√' \
	#'\cbrt=∛' \
	#'\inf=∞' \
	#'\infinito=∞' \
	#'\portanto=∴' \
	#'\entonces=⇒' \
	#'\thus=⇒' \
	#'\aprox=≈' \
	#'\noaprox=≉' \
	#'\union=∪' \
	#'\inter=∩' \
	#'\y=∧' \
	#'\o=∨' \
	#'\Union=⋃' \
	#'\Inter=⋂' \
	#'\Y=⋀' \
	#'\O=⋁' \
	#'\entre=÷' \
	#'\integral=∫' \
	#'\noigual=≠' \
	#'\incluido=⊆' \
	#'\noincluido=⊈' \
	#'\pertenece=∈' \
	#'\nopertenece=∉' \
	#'\rpertenece=∋' \
	#'\rnopertenece=∌' \
	#'\lincluido=⊆' \
	#'\rincluido=⊇' \
	#'\kronecker=⊗' \
	#'\cuadrado=■' \
	#'\square=■' \
	#'\cuadrado2=□' \
	#'\circulo=●' \
	#'\circle=●' \
	#'\circulo2=◯'

 #'|.'='|xargs ' # No funciona, ver ble/complete/sabbrev/expand

#FINAB

###########################
######## Modo Vim #########
###########################

# Puedes revisar los accesos rápidos con el comando "ble-bind -P"

## Modo vi, configuraciones personales:

# Modo normal
ble-bind -m 'vi_nmap' -f 'l' 'vi-command/forward-line'
ble-bind -m 'vi_nmap' -f 'ñ' 'vi-command/forward-char'
ble-bind -m 'vi_nmap' -f 'j' 'vi-command/backward-char'
#[ "$ONTTY" ] && ble-bind -m 'vi_imap' -f 'm ñ' 'vi_imap/normal-mode'
ble-bind -m 'vi_imap' -f 'm ñ' 'vi_imap/normal-mode'
ble-bind -m 'vi_nmap' -f 'w' 'vi-command/backward-vword'
ble-bind -m 'vi_nmap' -f 'W' 'vi-command/backward-uword'
ble-bind -m 'vi_nmap' -f 'e' 'vi-command/forward-vword'
ble-bind -m 'vi_nmap' -f 'E' 'vi-command/forward-uword'
ble-bind -m 'vi_nmap' -f 'b' 'vi-command/forward-vword-end'
ble-bind -m 'vi_nmap' -f 'B' 'vi-command/backward-vword-end'
ble-bind -m 'vi_nmap' -f ',' 'vi-command/search-char-repeat'
ble-bind -m 'vi_nmap' -f ';' 'vi-command/search-char-reverse-repeat'
ble-bind -m 'vi_nmap' -f 'h' 'vi-command/commandline'
ble-bind -m 'vi_nmap' -f 'U' 'vi_nmap/redo'
ble-bind -m 'vi_nmap' -f '-' 'vi-command/search-forward'
ble-bind -m 'vi_nmap' -f '+' 'vi-command/goto-mark'
ble-bind -m 'vi_imap' -f 'M-i' 'backward-char'
ble-bind -m 'vi_imap' -f 'M-j' 'backward-char'
ble-bind -m 'vi_imap' -f 'M-a' 'forward-char'
ble-bind -m 'vi_imap' -f 'M-ñ' 'forward-char'
ble-bind -m 'vi_imap' -f 'M-k' 'backward-line'
ble-bind -m 'vi_imap' -f 'M-l' 'forward-line'
ble-bind -m 'vi_imap' -f '¿' 'delete-forward-char'

# Modo visual
ble-bind -m 'vi_xmap' -f 'l' 'vi-command/forward-line'
ble-bind -m 'vi_xmap' -f 'ñ' 'vi-command/forward-char'
ble-bind -m 'vi_xmap' -f 'j' 'vi-command/backward-char'
#[ "$ONTTY" ] && ble-bind -m 'vi_xmap' -f 'm ñ' 'vi_xmap/exit'
ble-bind -m 'vi_xmap' -f 'm ñ' 'vi_xmap/exit'
ble-bind -m 'vi_xmap' -f 'w' 'vi-command/backward-vword'
ble-bind -m 'vi_xmap' -f 'W' 'vi-command/backward-uword'
ble-bind -m 'vi_xmap' -f 'e' 'vi-command/forward-vword'
ble-bind -m 'vi_xmap' -f 'E' 'vi-command/forward-uword'
ble-bind -m 'vi_xmap' -f 'b' 'vi-command/forward-vword-end'
ble-bind -m 'vi_xmap' -f 'B' 'vi-command/backward-vword-end'
ble-bind -m 'vi_xmap' -f ',' 'vi-command/search-char-repeat'
ble-bind -m 'vi_xmap' -f ';' 'vi-command/search-char-reverse-repeat'
ble-bind -m 'vi_xmap' -f 'h' 'vi-command/commandline'
ble-bind -m 'vi_xmap' -f '-' 'vi-command/search-forward'
ble-bind -m 'vi_xmap' -f '+' 'vi-command/goto-mark'

# Para expandir abreviaciones con Alt . (no funciona?)
ble-bind -f 'M-.' 'sabbrev-expand'

# Usar teclas vim en menú de completado
#ble-bind -f 'j' 'menu-complete backward'

# Para completar auto sugerencias con Alt ñ
ble-bind -m 'auto_complete' -f 'M-ñ' 'auto_complete/insert-on-end'

# Para remover auto sugerencias con Alt {
ble-bind -m 'isearch' -f 'M-{' 'isearch/cancel'

## The following setting specifies the input encoding. Currently only "UTF-8"
## and "C" is available.

bleopt input_encoding=UTF-8


## The following setting specifies the pager used by ble.sh.  This is used to
## show the help of commands (f1).

#bleopt pager=less

## The following setting specifies the editor used by ble.sh.  This is used for
## the widget edit-and-execute (C-x C-e) and editor for a large amount of
## command line texts.  Possible values are e.g. "vim", "emacs -nw" or "nano".

bleopt editor=nvim


## The following settings sets the behavior of visible bells (vbell).
## "vbell_duration" sets the time duration to show the vbell. "vbell_align"
## controls the position of vbell. The value "left" (default) specifies that
## the vbell should be shown up on the top left corner of the terminal. The
## values "center" and "right" specify that the vbell is shown on the top
## center and the top right corner, respectively.

#bleopt vbell_default_message=' Wuff, -- Wuff!! '
#bleopt vbell_duration=2000
#bleopt vbell_align=right


##-----------------------------------------------------------------------------
## Line editor settings


## The following settings turns on/off the audible bells and visible bells for
## errors while user edit. The non-empty value turns on the bells.

#bleopt edit_abell=1
#bleopt edit_vbell=


## The following setting turns on the delayed load of history when an non-empty
## value is set.

bleopt history_lazyload=1


## The following setting turns on the delete selection mode when an non-empty
## value is set. When the mode is turned on the selection is removed when a
## user inserts a character.

#bleopt delete_selection_mode=1


## The following settings control the indentation. "indent_offset" sets the
## indent width. "indent_tabs" controls if tabs can be used for indentation or
## not. If "indent_tabs" is set to 0, tabs will never be used. Otherwise
## indentation is made with tabs and spaces.

bleopt indent_offset=4
#bleopt indent_tabs=1


## "undo_point" controls the cursor position after "undo". When "beg" or "end"
## is specified, the cursor will be moved to the beginning or the end of the
## dirty section, respectively. When other values are specified, the original
## cursor position is reproduced.

#bleopt undo_point=end


## The following setting controls forced layout calculations before graphical
## operations. When a non-empty value is specified, the forced calculations are
## enabled. When an empty string is set, the operations are switched to logical
## ones.

#bleopt edit_forced_textmap=1


## The following option controls the interpretation of lines when going to the
## beginning or the end of the current line.  When the value `logical` is
## specified, the logical line is used, i.e., the beginning and the end of the
## line is determined based on the newline characters in the edited text.  When
## the value `graphical` is specified, the graphical line is used, i.e., the
## beginning and the end of the displayed line in the terminal is used.

bleopt edit_line_type=graphical


#bleopt edit_magic_expand=history:sabbrev:alias  # Expandir aliases como abreviaciones

## The following option controls the position of the info pane where completion
## menu, mode names, and other information are shown.  When the value "top" is
## specified, the info pane is shown just below the command line.  When the
## value "bottom" is specified, the info pane is shown at the bottom of the
## terminal.  The default is "top".

#bleopt info_display=bottom
bleopt info_display=top


################################
##### Indicador izquierdo ######
################################

## The following settings controls the prompt after the cursor left the command
## line.  "prompt_ps1_final" contains a prompt string.  "prompt_ps1_transient"
## is a colon-separated list of fields "always", "same-dir" and "trim".  The
## prompt is replaced by "prompt_ps1_final" if it has a non-empty value.
## Otherwise, the prompt is trimmed leaving the last line if
## "prompt_ps1_transient" has a field "trim".  Otherwise, the prompt vanishes
## if "prompt_ps1_transient" has a non-empty value.  When
## "prompt_ps1_transient" contains a field "same-dir", the setting of
## "prompt_ps1_transient" is effective only when the current working directory
## did not change since the last command line.

#bleopt prompt_ps1_final="\033[48;2;35;38;39;38;2;28;220;156m   "
#bleopt prompt_ps1_transient=trim

##############################
##### Indicador derecho ######
##############################

## The following settings controls the right prompt. "prompt_rps1" specifies
## the contents of the right prompt in the format of PS1.  When the cursor
## leaves the current command line, the right prompt is replaced by
## "prompt_rps1_final" if it has a non-empty value, or otherwise, the right
## prompt vanishes if "prompt_rps1_transient" is set to a non-empty value,

# Esta prompt pone un ✓ en caso el comando fue exitoso
# o una ✗ con el estado de salida en caso no fue exitoso

#_es() {
    #_ES=$?
    #[[ $_ES != 0 ]] && \
    #printf "\033[1;38;5;208m$_ES\033[38;5;1m\342\234\227 " || \
    #printf "\033[38;5;47m\342\234\223 "
#}

#blehook PRECMD='_es'
#bleopt prompt_rps1=$(_es)
bleopt prompt_rps1='$(_ES=$?; [[ $_ES -ne 0 ]] && printf "\[\033[38;5;235m\]\[\033[1;38;5;208;48;5;235m\] $_ES\[\033[38;5;1;48;5;235m\]\342\234\227" || printf "\[\033[38;5;235m\]\[\033[1;38;5;47;48;5;235m\] \342\234\223") '
#bleopt prompt_rps1_final=''
#bleopt prompt_rps1_transient=''
alias rps1='bleopt prompt_rps1'


## The following settings specify the content of terminal titles and status
## lines.  "prompt_xterm_title" specifies the terminal title which can be set
## by "OSC 0 ; ... BEL".  "prompt_screen_title" is effective inside terminal
## multiplexers such as GNU screen and tmux and specifies the window title of
## the terminal multiplexer which can be set by "ESC k ... ST".
## "prompt_term_status" is only effective when terminfo entries "tsl" and "fsl"
## (or termcap entries "ts" and "ds") are available, and specifies the content
## of the status line which can be set by the terminfo entries.  When each
## setting has non-empty value, the content of corresponding title or status
## line is replaced just before PS1 is shown.

#bleopt prompt_xterm_title=
#bleopt prompt_screen_title=
#bleopt prompt_term_status=


## The following settings control the status line.  "prompt_status_line"
## specifies the content of the status line.  If its value is empty, the status
## line is not shown.  "prompt_status_align" controls the position of the
## content in the status line.

#bleopt prompt_status_line="\e[4mThis is my bar"
#bleopt prompt_status_align=right

# Esta es una función para un nuevo PWD "$NEW_PWD", que será llamada en cada cambio de directorio
precmd_new_pwd() {
  # Cantidad de caracteres máximos en la prompt,
  # Símbolo en caso se trunque,
  # Directorio local
  local \
	pwdmaxlen=70 \
	trunc_symbol="…" \
	dir=${PWD##*/}

  # Cuál longitud usar
  pwdmaxlen=$(( ( pwdmaxlen < ${#dir} ) ? ${#dir} : pwdmaxlen ))

  NEW_PWD=${PWD/#$HOME/\}

  local pwdoffset=$(( ${#NEW_PWD} - pwdmaxlen ))

  # Generar el nombre
  if [ ${pwdoffset} -gt "0" ]
  then
  NEW_PWD=${NEW_PWD:$pwdoffset:$pwdmaxlen} \
  NEW_PWD=${trunc_symbol}/${NEW_PWD#*/}
  fi
}

precmd_new_pwd                   # Ejecuto la función antes del primer indicador
blehook CHPWD+=precmd_new_pwd    # Agrego esa función a este gancho de ble

# Esta es una función de línea de estado
# Ble también tiene un módulo vim-airline

## function algo/set-up-status-line {
## 
##   # Hide the default mode name in the info pane
##   bleopt keymap_vi_mode_show=
## 
##   # Update prompts on the mode change
##   bleopt keymap_vi_mode_update_prompt=1
## 
##   # Define a named prompt sequence \q{algo/vim-mode}
##   function ble/prompt/backslash:algo/vim-mode {
##     local mode; ble/keymap:vi/script/get-mode
##     case $mode in
##     (*n)  ble/prompt/print $'\e[1;37;48;2;127;0;93m N \e[m' ;;
##     (*v)  ble/prompt/print $'\e[1;37;48;2;127;0;93m V \e[m' ;;
##     (*V)  ble/prompt/print $'\e[1;37;48;2;127;0;93m V \e[m' ;;
##     (*^V) ble/prompt/print $'\e[1;37;48;2;127;0;93m V \e[m' ;;
##     (*s)  ble/prompt/print $'\e[1;37;48;2;127;0;93m S \e[m' ;;
##     (*S)  ble/prompt/print $'\e[1;37;48;2;127;0;93m S \e[m' ;;
##     (*^S) ble/prompt/print $'\e[1;37;48;2;127;0;93m S \e[m' ;;
##     (i)   ble/prompt/print $'\e[1;37;48;2;127;0;93m I \e[m' ;;
##     (R)   ble/prompt/print $'\e[1;37;48;2;127;0;93m R \e[m' ;;
##     (^R)  ble/prompt/print $'\e[1;37;48;2;127;0;93m R \e[m' ;;
##     (*)   ble/prompt/print $'\e[1;37;48;2;127;0;93m ?????? \e[m' ;;
##     esac
##   }
## 
##   #ble-color-setface prompt_status_line none
##   ble-color-setface prompt_status_line        fg=231,bg=0
##   local primer_triangulo="\e[40;38;2;127;0;93;48;2;198;71;20m"     ##7f00d1
##   local segundo_triangulo="\e[38;2;198;71;20;48;2;50;50;50m"       ##C64714
##   local ultimo_triangulo="\e[38;2;50;50;50;40m"
##   bleopt prompt_status_line='\q{algo/vim-mode}'"${primer_triangulo}${segundo_triangulo}"'\e[37m ${NEW_PWD} '"${ultimo_triangulo}"'\e[m'
## }
## blehook/eval-after-load keymap_vi algo/set-up-status-line


## "prompt_eol_mark" specifies the contents of the mark used to indicate the
## command output is not ended with newlines. The value can contain ANSI escape
## sequences.

bleopt prompt_eol_mark=$'\e[1;31m⏎ \e[m'

## "exec_errexit_mark" specifies the format of the mark to show the exit status
## of the command when it is non-zero.  If this setting is an empty string the
## exit status will not be shown.  The value can contain ANSI escape sequences.

#bleopt exec_errexit_mark=$'\e[91m[ble: exit %d]\e[m'

#Esto deshabilita el mensaje de estado de salida:
bleopt exec_errexit_mark=''

# Mensaje de tiempo transcurrido de ejecución de comando
bleopt exec_elapsed_mark=$'\e[94m[%ss (%s %%)]\e[m'
# Tiempo para mensaje de transcurso de tiempo
bleopt exec_elapsed_enabled='sys+usr>=2*60*1000'

## The following setting controls the exit when jobs are remaining. When an
## empty string is set, the shell will never exit with remaining jobs through
## widgets. When an non-empty value is set, the shell will exit when exit is
## attempted twice consecutively.

#bleopt allow_exit_with_jobs=


## The following setting controls the cursor position after the move to other
## history entries. When non-empty values are specified, the offset of the
## cursor from the beginning of the command line is preserved. When an empty
## value is specified the cursor position is the beginning or the end of the
## command lines when the move is to a newer or older entry, respectively.

#bleopt history_preserve_point=


## The following setting controls the history sharing. If it has non-empty
## value, the history sharing is enabled. With the history sharing, the command
## history is shared with the other Bash ble.sh sessions with the history
## sharing turned on.

#bleopt history_share=


## The following setting controls the behavior of the widget
## "accept-single-line-or-newline" in the single-line editing mode. The value
## is a subject of arithmetic evaluation. When it evaluates to negative
## integers, the line is always accepted. When it evaluates to 0, it enters the
## multiline editing mode when there is any unprocessed user inputs, or
## otherwise the line is accepted. When it evaluates to a positive integer "n",
## it enters the multiline editing mode when there is more than "n"unprocessed
## user inputs.

#bleopt accept_line_threshold=5


## The following option controls the behavior when the number of characters
## exceeds the capacity specified by `line_limit_length`.  The value `none`
## means that the number of characters will not be checked.  The value
## `discard` means that the characters cannot be inserted when the number of
## characters exceeds the capacity.  The value `truncate` means that the
## command line is truncated from its end to fit into the capacity.  The value
## `editor` means that the widget `edit-and-execute` will be invoked to open an
## editor to edit the command line contents.

#bleopt line_limit_type=truncate


## The following option specifies the capacity of the command line in the
## number of characters.  The number 0 or negative numbers means the unlimited
## capacity.

#bleopt line_limit_length=${COLUMNS}


## The following option specifies the maximal number of characters which can be
## appended into the history.  When this option has a positive value, commands
## with the length longer than the value is not appended to the history.  When
## this option has a non-positive value, commands are always appended to the
## history regardless of their length.

#bleopt history_limit_length=10000


##-----------------------------------------------------------------------------
## Terminal state control


## The following setting specifies the cursor type when commands are executed.
## The cursor type is specified by the argument of the control function
## DECSCUSR.

bleopt term_cursor_external=0


## The following settings, external and internal, specify the "modifyOtherKeys"
## states [the control function SM(>4)] when commands are executed and when
## ble.sh has control, respectively.

#bleopt term_modifyOtherKeys_external=auto
#bleopt term_modifyOtherKeys_internal=auto


##-----------------------------------------------------------------------------
## Rendering options


## "tab_width" specifies the width of TAB on the command line. When an empty
## value is specified, the width in terminfo (tput it) is used.

bleopt tab_width=4


## "char_width_mode" specifies the width of East_Asian_Width=A characters.
## When the value "east" is specified, the width is 2. When the value "west"
## is specified, the width is 1. When "auto" is specified, "east" or "west"
## is automatically selected based on interactions with the terminal. When
## the value "emacs" is specified, the width table (depending on characters)
## used in Emacs is used.

bleopt char_width_mode=auto


## "emoji_width" specifies the width of emoji characters.  If an empty value is
## specified, special treatment of emoji is disabled.

bleopt emoji_width=2


## "emoji_version" specifies the version of Unicode Emoji.  Available values
## are 1.0, 2.0, 3.0, 4.0, 5.0, 11.0, 12.0, 12.1, 13.0, and 13.1.

bleopt emoji_version=13.0


##-----------------------------------------------------------------------------
## User input settings


## The following setting sets the default keymap. The value "emacs" specifies
## that the emacs keymap should be used. The value "vi" specifies that the vi
## keymap (insert mode) should be used as the default. The value "auto"
## specifies that the keymap should be automatically selected from "emacs" or
## "vi" according to the current readline state "set -o emacs" or "set -o vi".

bleopt default_keymap=vi
#bleopt default_keymap=emacs


## The following setting controls the treatment of isolated ESCs.  The value
## "esc" indicates that it should be treated as ESC.  The value "meta"
## indicates that it should be treated as Meta modifier.  The value "auto"
## indicates that the behavior will be switched to an appropriate side of "esc"
## or "meta" depending on the current keymap.

#bleopt decode_isolated_esc=esc


## The following setting specifies the byte code used to abort the currently
## processed inputs. The default value 28 corresponds to "C-\".

#bleopt decode_abort_char=28


## The following settings sets up the behavior for errors while user input
## decoding. "error_char" is the decoding error for the current character
## encoding. "error_cseq" indicates the unrecognized CSI sequences.
## "error_kseq" indicates the unbound key sequences. "abell" and "vbell" turn
## on/off the audible bells and visible bells on errors. If the variable is
## empty the bells are turned off, or otherwise turned on. "discard" controls
## if the chars/sequences will be discarded or processed in later stage. If a
## non-empty value is given, chars/sequences are discarded.

#bleopt decode_error_char_abell=
#bleopt decode_error_char_vbell=1
#bleopt decode_error_char_discard=
#bleopt decode_error_cseq_abell=
#bleopt decode_error_cseq_vbell=1
#bleopt decode_error_cseq_discard=1
#bleopt decode_error_kseq_abell=1
#bleopt decode_error_kseq_vbell=1
#bleopt decode_error_kseq_discard=1


##-----------------------------------------------------------------------------
## Settings for completion


## The following settings turn on/off the corresponding functionalities. When
## non-empty strings are set, the functionality is enabled. Otherwise, the
## functionality is inactive.

bleopt complete_auto_complete=1
bleopt complete_menu_complete=1
bleopt complete_menu_filter=1

## Esta configuración permite autocompletado insensible a mayúsculas

bind 'set completion-ignore-case on'

## If "complete_ambiguous" has non-empty values, ambiguous completion
## candidates are generated for completion.

#bleopt complete_ambiguous=1


## If "complete_contract_function_names" has non-empty values, the function
## name candidates are grouped by prefixes of the directory-like form "*/".

#bleopt complete_contract_function_names=1


## By default, ble.sh does not allow rewriting the existing text if non-unique
## candidates does not contain the existing text.  If this setting has
## non-empty values, ble.sh rewrites the existing text.

#bleopt complete_allow_reduction=1


## If "complete_auto_history" has non-empty values, auto-complete searches
## matching command lines from history.

bleopt complete_auto_history=0


## The following setting controls the delay of auto-complete after the last
## user input. The unit is millisecond.

bleopt complete_auto_delay=0


## The setting "complete_auto_wordbreaks" is used as the delimiters for
## identifying words for M-right (auto-complete/insert-word).  The default
## value is $' \t\n'.  If the empty value is set, the default value is used.

#bleopt complete_auto_wordbreaks=$' \t\n/'


## The setting "complete_auto_menu" controls the delay of "auto-menu".  When a
## non-empty string is set, auto-menu is enabled.  The string is evaluated as
## an arithmetic expression to give the delay in milliseconds.  ble.sh will
## automatically show the menu of completions after the idle time (for which
## user input does not arrive) reaches the delay.

#bleopt complete_auto_menu=1
#bleopt complete_auto_menu=5000


## When there are user inputs while generating completion candidates, the
## candidates generation will be canceled to process the user inputs. The
## following setting controls the interval of checking user inputs while
## generating completion candidates.

#bleopt complete_polling_cycle=1
#bleopt complete_polling_cycle=50


## A hint on the maximum acceptable size of any data structure generated during
## the completion process, beyond which the completion may be prematurely
## aborted to avoid excessive processing time.  "complete_limit" is used for
## TAB completion.  When its value is empty, the size checks are disabled.
## "complete_limit_auto" is used for auto-completion.  When its value is empty,
## the setting "complete_limit" is used instead.

bleopt complete_limit=300
bleopt complete_limit_auto=200


## The following setting controls the timeout for the pathname expansions
## performed in auto-complete.  When the word contains a glob pattern that
## takes a long time to evaluate the pathname expansion, auto-complete based on
## the filename is canceled based on the timeout setting.  The value specifies
## the timeout duration in milliseconds.  When the value is empty, the
## timeout is disabled.

# IMPORTANTE: Establecerlo, para que en directorios grandes, no se trabe
 bleopt complete_timeout_auto=3000


## The following setting controls the timeout for the pathname expansions to
## prepare COMP_WORDS and COMP_LINE for progcomp.  When the word contains a
## glob pattern that takes a long time to evaluate, the pathname expansion is
## canceled, and a noglob expansion is used to construct COMP_WORDS and
## COMP_LINE.  The value specifies ## the timeout duration in milliseconds.
## When the value is empty, the timeout is disabled.

 bleopt complete_timeout_compvar=200


## The following setting specifies the style of the menu to show completion
## candidates. The value "dense" and "dense-nowrap" shows candidates separated
## by spaces. "dense-nowrap" is different from "dense" in the behavior that it
## inserts a new line before the candidates that does not fit into the
## remaining part of the current line. The value "align" and "align-nowrap"
## aligns the candidates. The value "linewise" shows a candidate per line.  The
## value "desc" and "desc-raw" shows a candidate per line with description for
## each. "desc-raw" is different from "desc" in the behavior that it interprets
## ANSI escape sequences in the descriptions.

bleopt complete_menu_style="align"


## The following setting specifies the maximal align width for
## complete_menu_style="align" and "align-nowrap".

## When a non-empty value is specified to this setting, the highlighting of the
## menu items is enabled.  This setting is synchronized with the readline
## variable "colored-stats".

#bleopt complete_menu_color=on


## When a non-empty value is specified to this setting, the part of the menu
## items matching with the already input text is highlighted.  This setting is
## synchronized with the readline variable "colored-completion-prefix".

#bleopt complete_menu_color_match=on

bleopt menu_align_min=4
bleopt menu_align_max=22


## The following setting specifies the maximal height of the menu.  When this
## value is evaluated to be a positive integer, the maximal line number of the
## menu is limited to that value.

#bleopt complete_menu_maxlines=3


## The following setting controls the detailed behavior of menu-complete with a
## colon-separated list of options:
##
## - When the option "insert-selection" is specified, the currently selected
##  menu item is temporarily inserted in the command line.

#bleopt complete_menu_complete_opts=insert-selection

## The following setting specifies the prefix for complete_menu_style=linewise.
## For example, the candidate number can be shown by setting the value "%d".
## ANSI escape sequences can be used.

#bleopt menu_linewise_prefix=%d


##-----------------------------------------------------------------------------
## Color settings

## The setting "term_index_colors" specifies the number of index colors
## supported by you terminal.  The value 256 is treated as xterm 256-color
## palette (16 basic + 6x6x6 color cube + 24 gray scale). The value 88 is
## treated as xterm 88-color palette (16 basic + 4x4x4 color cube + 8 gray
## scale).  The value 0 means that the terminal does not support extra colors
## other than basic colors.

# bleopt term_index_colors=256


## The setting "term_true_colors" specifies the format of 24-bit color escape
## sequences supported by your terminal.  The value "semicolon" indicates the
## format "CSI 3 8 ; 2 ; R ; G ; B m".  The value "colon" indicates the format
## "CSI 3 8 : 2 : R : G : B m".  The other value implies that the terminal does
## not support 24-bit color sequences.  In this case, ble.sh tries to output
## indexed color sequences or basic color sequences with properly reduced
## colors.

# bleopt term_true_colors=semicolon


## The setting "filename_ls_colors" can be used to import the filename coloring
## scheme by the environment variable LS_COLORS.

# bleopt filename_ls_colors="$LS_COLORS"


## The following settings enable or disable the syntax highlighting.  When the
## setting "highlight_syntax" has a non-empty value, the syntax highlighting is
## enabled.  When the setting "highlight_filename" has a non-empty value, the
## highlighting based on the filename and the command name is enabled during
## the process of the syntax highlighting.  Similarly, when the setting
## "highlight_variable" has a non-empty value, the highlighting based on the
## variable type is enabled.  All of these settings have non-empty values by
## default.

# bleopt highlight_syntax=
# bleopt highlight_filename=
# bleopt highlight_variable=


## The following settings control the timeout and user-input cancellation of
## the pathname expansions performed in the syntax highlighting.  When the word
## contains a glob pattern that takes a long time to evaluate the pathname
## expansion, the syntax highlighting based on the filename is canceled based
## on the timeouts specified by these settings.  "highlight_timeout_sync" /
## "highlight_timeout_async" specify the timeout durations in milliseconds to
## be used for the foreground / background syntax highlighting, respectively.
## When the timeout occurred in the foreground, the syntax highlighting will be
## deferred to the background syntax highlighting.  When the timeout occurred
## in the background, the syntax highlighting for the filename is canceled.
## When the value is empty, the corresponding timeout is disabled.
## "syntax_eval_polling_interval" specifies the maximal interval between the
## user-input checking.

# bleopt highlight_timeout_sync=500
# bleopt highlight_timeout_async=5000
# bleopt syntax_eval_polling_interval=50


##########################################################
########### Colores de resaltado de sintaxis #############
##########################################################

# Puedes ver los colores con el comando "ble-color-setface"

ble-face -s region                    fg=white,bg=60
ble-face -s region_insert             fg=#6B6E6
# ble-face -s region_match              fg=white,bg=55
# ble-face -s region_target             fg=black,bg=153
# ble-face -s disabled                  fg=242
# ble-face -s overwrite_mode            fg=black,bg=51
case $TERM in
    screen|*tmux*) ble-face -s auto_complete  fg=#6B6E6E,underline ;;
    *)  ble-face -s auto_complete  fg=#6B6E6E ;;
esac

# Estos 2 ↓ no se encuentran cuando se activa vim-airline, reportar como bug
#ble-face -s menu_filter_fixed         bold,underline
#ble-face -s menu_filter_input         fg=229,bg=16

ble-face -s vbell                     fg=green,bold,italic
ble-face -s vbell_erase               fg=#000
ble-face -s vbell_flash               fg=red,reverse,bold,italic
#ble-face -s prompt_status_line        fg=231,bg=240

# ble-face -s syntax_default            none
# ble-face -s syntax_command            fg=brown
ble-face -s syntax_quoted             fg=green,italic
# ble-face -s syntax_quotation          fg=green,bold
ble-face -s syntax_expr               fg=9
#ble-face -s syntax_error              fg=#ff9500,bold,underline
ble-face -s syntax_error              fg=red,bold,underline
ble-face -s argument_error            fg=yellow,bold
ble-face -s syntax_varname            fg=#ff9500,bold
ble-face -s syntax_delimiter          fg=220
# ble-face -s syntax_param_expansion    fg=purple
ble-face -s syntax_history_expansion  fg=#A017A2
ble-face -s syntax_function_name      fg=#9F6DE4,bold
ble-face -s syntax_comment            fg=#7f7f7f,italic
# ble-face -s syntax_glob               fg=198,bold
# ble-face -s syntax_brace              fg=37,bold
# ble-face -s syntax_tilde              fg=navy,bold
ble-face -s syntax_document           fg=#54ff54,italic
ble-face -s syntax_document_begin     fg=#ff004c,bold
# ble-face -s command_builtin_dot       fg=red,bold
ble-face -s command_builtin           fg=red
ble-face -s command_alias             fg=51
ble-face -s command_function          fg=purple # fg=purple
ble-face -s command_file              fg=#54ff54
ble-face -s command_keyword           fg=#F81E16,bold
# ble-face -s command_jobs              fg=red,bold
ble-face -s command_directory         fg=#438eff,underline
ble-face -s filename_directory        fg=#438eff,underline
# ble-face -s filename_directory_sticky underline,fg=white,bg=26
ble-face -s filename_link             underline,fg=#00e5ff
ble-face -s filename_orphan           fg=teal,bg=240,underline
# ble-face -s filename_setuid           underline,fg=black,bg=220
# ble-face -s filename_setgid           underline,fg=black,bg=191
# ble-face -s filename_executable       underline,fg=green
# ble-face -s filename_other            underline
ble-face -s filename_socket           underline,fg=13
ble-face -s filename_pipe             underline,fg=orange
ble-face -s filename_character        underline,fg=11
ble-face -s filename_block            underline,fg=11
# ble-face -s filename_warning          underline,fg=red
ble-face -s filename_url              underline,italic,fg=#FF613D
# ble-face -s filename_ls_colors        underline
# ble-face -s varname_array             fg=orange,bold
# ble-face -s varname_empty             fg=31
# ble-face -s varname_export            fg=200,bold
# ble-face -s varname_expr              fg=92,bold
# ble-face -s varname_hash              fg=70,bold
ble-face -s varname_number            fg=11
# ble-face -s varname_readonly          fg=200
# ble-face -s varname_transform         fg=29,bold
ble-face -s varname_unset             fg=#ff004c

#################################
### Vim-airline configuración ###
#################################
# Habilitar esto me toma 0.006s en cada prompt
ble-import lib/vim-airline

ble/gdict#set _ble_lib_vim_airline_mode_map n N
ble/gdict#set _ble_lib_vim_airline_mode_map v V
ble/gdict#set _ble_lib_vim_airline_mode_map i I
ble/gdict#set _ble_lib_vim_airline_mode_map o OP
ble/gdict#set _ble_lib_vim_airline_mode_map s S
ble/gdict#set _ble_lib_vim_airline_mode_map R R

bleopt vim_airline_left_alt_sep=
bleopt vim_airline_left_sep=
bleopt vim_airline_right_alt_sep=
bleopt vim_airline_right_sep=
bleopt vim_airline_section_a='\e[1m\q{lib/vim-airline/mode}'
bleopt vim_airline_section_b='\q{lib/vim-airline/gitstatus}'
bleopt vim_airline_section_c='${NEW_PWD}'
bleopt vim_airline_section_x=''
#bleopt vim_airline_section_y='$_ble_util_locale_encoding[unix]'
bleopt vim_airline_section_y=''
#bleopt vim_airline_section_z=' \q{history-percentile} \e[1m!\q{history-index}/\!\e[22m \q{position}'
#bleopt vim_airline_section_z=''
bleopt vim_airline_section_z=''
bleopt vim_airline_symbol_branch=
bleopt vim_airline_symbol_dirty=⚡
#bleopt vim_airline_theme=dark

ble-color-setface vim_airline_a                            fg=15,bg=53
ble-color-setface vim_airline_a_commandline                fg=17,bg=40
ble-color-setface vim_airline_a_commandline_modified       ref:vim_airline_a_commandline
ble-color-setface vim_airline_a_inactive                   fg=239,bg=234
ble-color-setface vim_airline_a_inactive_modified          ref:vim_airline_a_inactive
#ble-color-setface vim_airline_a_insert                     bg=53,fg=15
ble-color-setface vim_airline_a_insert bg=89,fg=15
ble-color-setface vim_airline_a_insert_modified            ref:vim_airline_a_insert
ble-color-setface vim_airline_a_modified                   ref:vim_airline_a
#ble-color-setface vim_airline_a_normal                     fg=17,bg=190
ble-color-setface vim_airline_a_normal fg=15,bg=18
ble-color-setface vim_airline_a_normal_modified            ref:vim_airline_a_normal
ble-color-setface vim_airline_a_replace                    ref:vim_airline_a_insert
ble-color-setface vim_airline_a_replace_modified           ref:vim_airline_a_replace
#ble-color-setface vim_airline_a_visual                     fg=16,bg=214
ble-color-setface vim_airline_a_visual bg=233,fg=15
ble-color-setface vim_airline_a_visual_modified            ref:vim_airline_a_visual
ble-color-setface vim_airline_b                            fg=231,bg=27
ble-color-setface vim_airline_b_commandline                fg=231,bg=238
ble-color-setface vim_airline_b_commandline_modified       ref:vim_airline_b_commandline
ble-color-setface vim_airline_b_inactive                   fg=239,bg=235
ble-color-setface vim_airline_b_inactive_modified          ref:vim_airline_b_inactive
#ble-color-setface vim_airline_b_insert                     fg=231,bg=27
ble-color-setface vim_airline_b_insert bg=202,fg=15,bold
ble-color-setface vim_airline_b_insert_modified            ref:vim_airline_b_insert
ble-color-setface vim_airline_b_modified                   ref:vim_airline_b
#ble-color-setface vim_airline_b_normal                     fg=231,bg=238
ble-color-setface vim_airline_b_normal fg=15,bg=1,bold
ble-color-setface vim_airline_b_normal_modified            ref:vim_airline_b_normal
ble-color-setface vim_airline_b_replace                    ref:vim_airline_b_insert
ble-color-setface vim_airline_b_replace_modified           ref:vim_airline_b_replace
#ble-color-setface vim_airline_b_visual                     fg=16,bg=orange
ble-color-setface vim_airline_b_visual bg=17,fg=15,bold
ble-color-setface vim_airline_b_visual_modified            ref:vim_airline_b_visual
ble-color-setface vim_airline_c                            fg=231,bg=18
ble-color-setface vim_airline_c_commandline                fg=158,bg=234
ble-color-setface vim_airline_c_commandline_modified       ref:vim_airline_c_commandline
ble-color-setface vim_airline_c_inactive                   fg=239,bg=236
ble-color-setface vim_airline_c_inactive_modified          ref:vim_airline_c_inactive
#ble-color-setface vim_airline_c_insert                     fg=231,bg=18
ble-color-setface vim_airline_c_insert fg=254,bg=237
ble-color-setface vim_airline_c_insert_modified            ref:vim_airline_c_insert
ble-color-setface vim_airline_c_modified                   ref:vim_airline_c
#ble-color-setface vim_airline_c_normal                     fg=158,bg=234
ble-color-setface vim_airline_c_normal fg=254,bg=237
ble-color-setface vim_airline_c_normal_modified            ref:vim_airline_c_normal
ble-color-setface vim_airline_c_replace                    ref:vim_airline_c_insert
ble-color-setface vim_airline_c_replace_modified           ref:vim_airline_c_replace
#ble-color-setface vim_airline_c_visual                     fg=231,bg=52
ble-color-setface vim_airline_c_visual bg=124,fg=15
ble-color-setface vim_airline_c_visual_modified            ref:vim_airline_c_visual
ble-color-setface vim_airline_error                        fg=16,bg=88
ble-color-setface vim_airline_error_commandline            ref:vim_airline_error
ble-color-setface vim_airline_error_commandline_modified   ref:vim_airline_error_commandline
ble-color-setface vim_airline_error_inactive               ref:vim_airline_error
ble-color-setface vim_airline_error_inactive_modified      ref:vim_airline_error_inactive
ble-color-setface vim_airline_error_insert                 ref:vim_airline_error
ble-color-setface vim_airline_error_insert_modified        ref:vim_airline_error_insert
ble-color-setface vim_airline_error_modified               ref:vim_airline_error
ble-color-setface vim_airline_error_normal                 ref:vim_airline_error
ble-color-setface vim_airline_error_normal_modified        ref:vim_airline_error_normal
ble-color-setface vim_airline_error_replace                ref:vim_airline_error_insert
ble-color-setface vim_airline_error_replace_modified       ref:vim_airline_error_replace
ble-color-setface vim_airline_error_visual                 ref:vim_airline_error
ble-color-setface vim_airline_error_visual_modified        ref:vim_airline_error_visual
ble-color-setface vim_airline_term                         fg=158,bg=234
ble-color-setface vim_airline_term_commandline             ref:vim_airline_term
ble-color-setface vim_airline_term_commandline_modified    ref:vim_airline_term_commandline
ble-color-setface vim_airline_term_inactive                ref:vim_airline_term
ble-color-setface vim_airline_term_inactive_modified       ref:vim_airline_term_inactive
ble-color-setface vim_airline_term_insert                  ref:vim_airline_term
ble-color-setface vim_airline_term_insert_modified         ref:vim_airline_term_insert
ble-color-setface vim_airline_term_modified                ref:vim_airline_term
ble-color-setface vim_airline_term_normal                  ref:vim_airline_term
ble-color-setface vim_airline_term_normal_modified         ref:vim_airline_term_normal
ble-color-setface vim_airline_term_replace                 ref:vim_airline_term_insert
ble-color-setface vim_airline_term_replace_modified        ref:vim_airline_term_replace
ble-color-setface vim_airline_term_visual                  ref:vim_airline_term
ble-color-setface vim_airline_term_visual_modified         ref:vim_airline_term_visual
ble-color-setface vim_airline_warning                      fg=16,bg=166
ble-color-setface vim_airline_warning_commandline          ref:vim_airline_warning
ble-color-setface vim_airline_warning_commandline_modified ref:vim_airline_warning_commandline
ble-color-setface vim_airline_warning_inactive             ref:vim_airline_warning
ble-color-setface vim_airline_warning_inactive_modified    ref:vim_airline_warning_inactive
ble-color-setface vim_airline_warning_insert               ref:vim_airline_warning
ble-color-setface vim_airline_warning_insert_modified      ref:vim_airline_warning_insert
ble-color-setface vim_airline_warning_modified             ref:vim_airline_warning
ble-color-setface vim_airline_warning_normal               ref:vim_airline_warning
ble-color-setface vim_airline_warning_normal_modified      ref:vim_airline_warning_normal
ble-color-setface vim_airline_warning_replace              ref:vim_airline_warning_insert
ble-color-setface vim_airline_warning_replace_modified     ref:vim_airline_warning_replace
ble-color-setface vim_airline_warning_visual               ref:vim_airline_warning
ble-color-setface vim_airline_warning_visual_modified      ref:vim_airline_warning_visual
ble-color-setface vim_airline_x                            ref:vim_airline_c
ble-color-setface vim_airline_x_commandline                ref:vim_airline_c_commandline
ble-color-setface vim_airline_x_commandline_modified       ref:vim_airline_x_commandline
ble-color-setface vim_airline_x_inactive                   ref:vim_airline_c_inactive
ble-color-setface vim_airline_x_inactive_modified          ref:vim_airline_x_inactive
#ble-color-setface vim_airline_x_insert                     ref:vim_airline_c_insert
ble-color-setface vim_airline_x_insert bg=242
ble-color-setface vim_airline_x_insert_modified            ref:vim_airline_x_insert
ble-color-setface vim_airline_x_modified                   ref:vim_airline_x
#ble-color-setface vim_airline_x_normal                     ref:vim_airline_c_normal
ble-color-setface vim_airline_x_normal bg=202,fg=232
ble-color-setface vim_airline_x_normal_modified            ref:vim_airline_x_normal
ble-color-setface vim_airline_x_replace                    ref:vim_airline_c_replace
ble-color-setface vim_airline_x_replace_modified           ref:vim_airline_x_replace
#ble-color-setface vim_airline_x_visual                     ref:vim_airline_c_visual
ble-color-setface vim_airline_x_visual bg=179,fg=232
ble-color-setface vim_airline_x_visual_modified            ref:vim_airline_x_visual
ble-color-setface vim_airline_y                            ref:vim_airline_b
ble-color-setface vim_airline_y_commandline                ref:vim_airline_b_commandline
ble-color-setface vim_airline_y_commandline_modified       ref:vim_airline_y_commandline
ble-color-setface vim_airline_y_inactive                   ref:vim_airline_b_inactive
ble-color-setface vim_airline_y_inactive_modified          ref:vim_airline_y_inactive
ble-color-setface vim_airline_y_insert                     ref:vim_airline_b_insert
ble-color-setface vim_airline_y_insert_modified            ref:vim_airline_y_insert
ble-color-setface vim_airline_y_modified                   ref:vim_airline_y
ble-color-setface vim_airline_y_normal                     ref:vim_airline_b_normal
ble-color-setface vim_airline_y_normal_modified            ref:vim_airline_y_normal
ble-color-setface vim_airline_y_replace                    ref:vim_airline_b_replace
ble-color-setface vim_airline_y_replace_modified           ref:vim_airline_y_replace
#ble-color-setface vim_airline_y_visual                     ref:vim_airline_b_visual
ble-color-setface vim_airline_y_visual bg=18,fg=white
ble-color-setface vim_airline_y_visual_modified            ref:vim_airline_y_visual
ble-color-setface vim_airline_z                            ref:vim_airline_a
ble-color-setface vim_airline_z_commandline                ref:vim_airline_a_commandline
ble-color-setface vim_airline_z_commandline_modified       ref:vim_airline_z_commandline
ble-color-setface vim_airline_z_inactive                   ref:vim_airline_a_inactive
ble-color-setface vim_airline_z_inactive_modified          ref:vim_airline_z_inactive
ble-color-setface vim_airline_z_insert                     ref:vim_airline_a_insert
ble-color-setface vim_airline_z_insert_modified            ref:vim_airline_z_insert
ble-color-setface vim_airline_z_modified                   ref:vim_airline_z
ble-color-setface vim_airline_z_normal                     ref:vim_airline_a_normal
ble-color-setface vim_airline_z_normal_modified            ref:vim_airline_z_normal
ble-color-setface vim_airline_z_replace                    ref:vim_airline_a_replace
ble-color-setface vim_airline_z_replace_modified           ref:vim_airline_z_replace
ble-color-setface vim_airline_z_visual                     ref:vim_airline_a_visual
ble-color-setface vim_airline_z_visual_modified            ref:vim_airline_z_visual

##-----------------------------------------------------------------------------
## Settings for vim-mode


function blerc/vim-load-hook {
  ((_ble_bash>=40300)) && builtin bind 'set keyseq-timeout 1'

  # bleopt keymap_vi_macro_depth=64
  # bleopt keymap_vi_mode_string_nmap:=$'\e[1m~\e[m'


  ## The setting "keymap_vi_mode_update_prompt" specifies that the prompt
  ## should be recalculated on the mode change.  When this option has a
  ## non-empty value, the prompt will be recalculated.

  # bleopt keymap_vi_mode_update_prompt=


  ## The option "keymap_vi_mode_show" controls if the mode line is shown.  When
  ## this option has a non-empty value, the mode line is shown.

  # bleopt keymap_vi_mode_show=1


  ## The following settings specify the name of modes in the mode line.

  #bleopt keymap_vi_mode_string_nmap=$'\e[1;37;48;2;39;0;138m NORMAL \e[40;38;2;39;0;138m\uE0B0\e[m'
  #bleopt keymap_vi_mode_name_replace=$'\e[1;37;48;2;39;0;138m REEMPLAZAR \e[40;38;2;39;0;138m\uE0B0\e[m'
  #bleopt keymap_vi_mode_name_vreplace=$'\e[1;37;48;2;39;0;138m VREEMPLAZAR \e[40;38;2;39;0;138m\uE0B0\e[m'
  #bleopt keymap_vi_mode_name_visual=$'\e[1;37;48;2;39;0;138m VREEMPLAZAR \e[40;38;2;39;0;138m\uE0B0\e[m'
  #bleopt keymap_vi_mode_name_select=$'\e[1;37;48;2;39;0;138m SELECCIONAR \e[40;38;2;39;0;138m\uE0B0\e[m'
  #bleopt keymap_vi_mode_name_linewise=$'\e[1;37;48;2;39;0;138m LINEA \e[40;38;2;39;0;138m\uE0B0\e[m'
  #bleopt keymap_vi_mode_name_blockwise=$'\e[1;37;48;2;39;0;138m BLOQUE \e[40;38;2;39;0;138m\uE0B0\e[m'


  ## The following setting sets up the keymap settings with Meta modifiers

  # ble-decode/keymap:vi_imap/define-meta-bindings

  ## Cursor settings

    ble-bind -m vi_nmap --cursor 2
    ble-bind -m vi_imap --cursor 5
    ble-bind -m vi_omap --cursor 4
    ble-bind -m vi_xmap --cursor 2
    ble-bind -m vi_cmap --cursor 0

  # Por si no quieres el "espacio mágico" que expande abreviaciones:
  #  ble-bind -m vi_imap -f 'SP' 'self-insert'

  ## DECSCUSR setting
  ##
  ##   If you don't have the entry Ss in terminfo, yet your terminal supports
  ##   DECSCUSR, please comment out the following line to enable DECSCUSR.
  ##
  #_ble_term_Ss=$'\e[@1 q'

  ## Control sequences that will be output on entering each mode
  # bleopt term_vi_nmap=
  # bleopt term_vi_imap=
  # bleopt term_vi_omap=
  # bleopt term_vi_xmap=
  # bleopt term_vi_cmap=

  ## vim-surround
  source "$_ble_base/lib/vim-surround.sh"
  #bleopt vim_surround_45:=$'$( \r )'
  #bleopt vim_surround_61:=$'$(( \r ))'

  ## vim-arpeggio
  #source "$_ble_base/lib/vim-arpeggio.sh"
  #bleopt vim_arpeggio_timeoutlen=10
  #ble/lib/vim-arpeggio.sh/bind -s jk 'hello'
}
blehook/eval-after-load keymap_vi blerc/vim-load-hook

##-----------------------------------------------------------------------------
## Internal settings

#bleopt internal_stackdump_enabled=0
#bleopt openat_base=30
#bleopt internal_exec_type=gexec
#bleopt internal_suppress_bash_output=1
#bleopt internal_ignoreeof_trap='Use "exit" to leave the shell.'
