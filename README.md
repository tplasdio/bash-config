<p align="center">
  <h1 align="center">bash-config</h1>
  <p align="center">
    My bash configurations
  </p>
  <img
    width="1200"
    src="https://codeberg.org/tplasdio/bash-config/raw/branch/main/media/bash-config.avif"
    alt="image not loaded"
  />
</p>

## Table of Contents

- [📰 Description](#description)
- [✨ Features](#features)
- [🚀 Setup](#setup)
  - [🏠 Runcom placing](#runcom-placing)
  - [📦 Installation](#installation)
- [⌨ Keybindings](#keybindings)
- [👀 See also](#see-also)
- [📝 Licence](#licence)

## 📰 Description

[Bash](https://www.gnu.org/software/bash/bash.html) is the
default command-line shell in most Linux distributions.
It is a powerful scripting language and interactive
shell that is nonetheless perceived as "old" and
"unfriendly" when compared to other modern shells.

These are my personal configurations that make it
on par or often better than other popular modern interactive
shells like `zsh` or `fish`.

## ✨ Features

- [Ble.sh](https://github.com/akinomyoga/ble.sh) features:
	- Syntax highlighting
	- Abbreviations
	- Vim editing mode
	- Menu completion based on man-pages
	- Right prompt with last exit status
	- Vim-airline-like status bar with `$PWD`
	- Other nice defaults that make it run relatively fast
- Useful interactive functions like:
	- `funced`: Search and edit interactive functions
	- `funcbat`: Print interactive function with syntax highlighting
	- `fhis`: Select a command in the bash history with `fzf`
	- `printz`: Insert command output as your next command-line
	- `unbind`: Unset a readonly variable
	- `cdf`: Select and `cd` to a directory in the `pushd` stack
	- `fcd`: Select and `cd` to a directory under the current one
	- `fcdd`: Select and `pushd` to a directory under the current one
	- `run_help`: Open manual of currently typed command, or select it with `fzf`
	- `run_copy`: Copy command-line to clipboard
	- `run_paste`: Paste clipboard to command-line
- Keybindings that integrate other useful terminal commands like:
	- [g.sh](https://codeberg.org/tplasdio/g.sh): Bookmark files
	- [z.lua](https://github.com/skywind3000/z.lua): Navigation to frecent directories
	- [lf](https://github.com/gokcehan/lf): Terminal file manager
	- [broot](https://github.com/Canop/broot): Fuzzy search file names
	- [rgfzf](https://codeberg.org/tplasdio/rgfzf): Fuzzy search file contents
	- [fman](https://codeberg.org/tplasdio/fman): Fuzzy select manuals and infodocs
	- [vidir](https://joeyh.name/code/moreutils/): Bulk file renaming
	- [cheat](https://github.com/cheat/cheat), [cht.sh](https://github.com/chubin/cheat.sh): Cheatsheets
- Modular
    - No "configuration framework", simple readable `source`s that you may remove to fit your needs.
    - Much comes from my `dash-config` written in POSIX shell and may be reused by other shells' configs
- No unnecesary function that is better off as a standalone shell script. 
- XDG directories setup
- Non-XDG directories fixing
- Pretty colors:
	- Setup color variables like `LS_COLORS`, `JQ_COLORS`, etc.
	- Colorizing of other commands with `grc` aliases.
	- Setup aliases for pretty colors commands like `lsd`, `grep`s, etc.

## 🚀 Setup

### 🏠 Runcom placing

Firstly, you will notice that these bash runcoms are
not in the usual places in the `$HOME` directory
`~/.{bashrc,bash_profile}`. Instead they
are in the `$XDG_CONFIG_HOME` directory as
`~/.config/bash/{bashrc,bash_profile}` because
this makes them better organized and doesn't
pollute your home directory.

To achieve this you simply need to put the
following at the end of your `/etc/bash.bashrc`:

```bash
BASHDOTDIR="${XDG_CONFIG_HOME:-"$HOME/.config"}/bash"
if [[ $(shopt login_shell) = *on* ]]; then
  [ -f "${BASHDOTDIR}/bash_profile" ] && . "${BASHDOTDIR}/bash_profile"
else
  [ -f "${BASHDOTDIR}/bashrc" ] && . "${BASHDOTDIR}/bashrc"
fi
```

and then move your files to the new locations:

```bash
mkdir -p ~/.config/bash
mv ~/.bashrc ~/.config/bash/bashrc
mv ~/.bash_profile ~/.config/bash/bash_profile
```

### 📦 Installation

- Install [ble.sh](https://github.com/akinomyoga/ble.sh) on `/usr/share/blesh/ble.sh`.
- Clone my [dash-config](https://codeberg.org/tplasdio/dash-config) repo and copy the directory `.config/dash` to your config directory.
- Clone this repo and copy the directories `.config/bash`, `.config/blesh` to your config directory.

The file tree structure I went for this config looks like this:

```
📁 ~/.config/bash
├── bashrc               ← bash interactive runcom
├── bashprofile          ← bash login runcom
├── 📁 variables         ← place for variables (regular and environmental), mainly for bashprofile
│   ├── xdg.sh           ← where you establish XDG Base Directory variables
│   ├── xdgfix.sh        ← where you fix non XDG Base Directory programs with env variables
│   ├── default_apps.sh  ← default application environment variables that I use in other apps
│   └── wrappers.sh      ← where I use a variable to the name of a wrapper script of another program
├── 📁 aliases           ← place for aliases
│   ├── main.sh          ← most of my aliases
│   └── xdgfix.sh        ← where you fix non XDG Base Directory programs with aliases
├── 📁 functions         ← place for functions, mainly interactive ones for bashrc
├── 📁 binds             ← place for keybindings, for readline and ble.sh integration with other commands
├── 📁 completions       ← place for completion functions
├── 📁 builtins          ← place for bash loadable builtins
├── 📁 prompt            ← place for left prompt code (PS1) or configuration for a prompt plugin
└── 📁 plugins           ← place for configuring programs with some extra bash integration

📁 ~/.config/blesh
└── init.sh              ← ble.sh configuration: keybindings, syntax highlighting colors, right prompt, statusline
```

Bear in mind that some aliases or functions reference various command-line programs
or my personal [scripts](https://codeberg.org/tplasdio/scripts) that you might
want to install or at least manually copy, like:
- [`lfcd`](https://codeberg.org/tplasdio/lf-config/src/branch/main/.config/dash/functions/lfcd),
- [`mpvsk`](https://codeberg.org/tplasdio/mpv-luasockets/src/branch/main/src/bin/mpvsk)
- [`ffd`](https://codeberg.org/tplasdio/scripts/src/branch/main/scripts/ffd)

## ⌨ Keybindings

*Note: I use a Spanish keyboard and my own custom vim-style bindings `j`,`k`,`l`,`ñ`*

Take a look at `binds/*` and `init.sh` for more details, but
here are the most important ones:

Keybinding|Description
------|------
`j`| ← in normal/visual mode
`k`| ↑ in normal/visual mode
`l`| ↓ in normal/visual mode
`ñ`| → in normal/visual mode
`i`| Enter insert mode
`mñ`| Back to normal mode
`alt j`| ← in insert mode
`alt k`| ↑ in insert mode
`alt l`| ↓ in insert mode
`alt ñ`| → in insert mode, or auto complete
`alt i`| ← in insert mode
`alt a`| → in insert mode
`w`| ← word in normal/visual mode
`W`| ← WORD in normal/visual mode
`B`| ← Word in normal/visual mode
`e`| → word in normal/visual mode
`E`| → WORD in normal/visual mode
`b`| → Word in normal/visual mode
`h`| command line
`alt h`| open `fman` man page chooser
`alt g`| print cheat sheet of command (local `cheat` and `cht.sh`)
`alt e`| open `$EDITOR` with ouput from command
`alt b`| open `vidir` bulk file renaming
`alt B`| open `vidir` bulk file renaming recursively
`alt .`| open `lf` file manager
`alt ,`| open `broot` file chooser
`alt ;`| open `fcd` directory chooser
`alt E`| open `ffd` file editing chooser
`alt z`| open `z.lua` frecent directory chooser
`alt H`| open `fhis` command history chooser
`alt r`| open `rgfzf` text file content searching
`alt R`| open `rgfzf` file content searching

## 👀 See also
- [ble.sh Wiki](https://github.com/akinomyoga/ble.sh/wiki).
- [XDG Base Directory](https://wiki.archlinux.org/index.php/XDG_Base_Directory).
- [bash-xdg](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=bash-xdg): Build of bash with XDG Base Directory support
- [dash-config](https://codeberg.org/tplasdio/dash-config): My `dash` configurations
- [lf-config](https://codeberg.org/tplasdio/lf-config): My `lf` configurations
- [broot-config](https://codeberg.org/tplasdio/broot-config): My `broot` configurations

## 📝 Licence
GLPv3 or later. ble.sh is under BSD-3clause
